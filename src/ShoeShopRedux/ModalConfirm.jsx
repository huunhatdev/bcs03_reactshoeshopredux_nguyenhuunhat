import React, { Component } from "react";
import { Modal } from "antd";
import { connect } from "react-redux";
import { OPEN_MODAL_CART, REMOVE_PRODUCT } from "./redux/constants/constants";

class ModalConfirm extends Component {
  render() {
    return (
      <Modal
        title="Bạn có muốn xoá sản phẩm này ???"
        visible={this.props.visible}
        onOk={() => {
          this.props.handleRemoveProduct(this.props.id);
          this.props.handleModal("close");
        }}
        onCancel={() => {
          this.props.handleModal("close");
        }}
      >
        <p>Sản phẩm sẽ bị xoá khỏi giỏ hàng !!!</p>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    visible: state.shoeShopReducer.isOpenModal,
    id: state.shoeShopReducer.idProductModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleModal: (action) => {
      dispatch({
        type: OPEN_MODAL_CART,
        payload: action,
      });
    },
    handleRemoveProduct: (id) => {
      dispatch({
        type: REMOVE_PRODUCT,
        payload: id,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalConfirm);
