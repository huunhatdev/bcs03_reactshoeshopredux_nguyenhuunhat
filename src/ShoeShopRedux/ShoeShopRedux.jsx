import React, { Component } from "react";
import { act } from "react-dom/test-utils";
import Cart from "./Cart";
import dataShoeShop from "./dataShoeShop.json";
import ProductList from "./ProductList";
import { connect } from "react-redux";

class ShoeShopRedux extends Component {
  render() {
    return (
      <div>
        <ProductList />
        <hr />
        <h4>Số lượng giỏ hàng: {this.props.totalCart}</h4>

        <Cart />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    totalCart: state.shoeShopReducer.cart.length,
  };
};

const mapDispatchToProps = () => {};
export default connect(mapStateToProps, mapDispatchToProps)(ShoeShopRedux);
