import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/constants";

class ProductItem extends Component {
  render() {
    let { product } = this.props;

    return (
      <div className="card-group col-3 mb-3">
        <div className="card">
          <img
            className="card-img-top"
            src={product.image}
            alt="Card image cap"
          />
          <div className="card-body text-left">
            <h4 className="card-title">{product.name}</h4>
            <p className="card-text text-danger">${product.price}</p>
            <button
              className="btn btn-info"
              onClick={() => this.props.handleAddToCart(product)}
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (product) => {
      dispatch({
        type: ADD_TO_CART,
        payload: product,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);
